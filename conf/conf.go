package conf

import (
	"fmt"
	"log"
	"os"

	"github.com/BurntSushi/toml"
	"github.com/gin-gonic/gin"
)

const PATH = "conf/config.toml"

//Config Struct
type Config struct {
	App App
	DB  MySQL `toml:"db"`
}

//Apps Struct
type App struct {
	Name, Description string
	Production        bool
	Port              int
}

//MySQL Struct
type MySQL struct {
	Driver, Connection, Host, Username, Password, Db, Charset string
	Port                                                      int
}

//GetCon From MySQL
func (m *MySQL) GetCon() string {

	con := fmt.Sprintf(
		"%s:%s@%s(%s:%d)/%s?charset=%s&parseTime=true&loc=Local",
		m.Username,
		m.Password,
		m.Connection,
		m.Host,
		m.Port,
		m.Db,
		m.Charset,
	)

	return con
}

//ReadConfig Config
func ReadConfig() Config {

	_, err := os.Stat(PATH)
	if err != nil {
		log.Panic(err)
	}

	var config Config
	if _, err := toml.DecodeFile(PATH, &config); err != nil {
		log.Panic(err)
	}

	return config
}

//ShareConfig Middleware
func ShareConfig(config Config) gin.HandlerFunc {

	return func(c *gin.Context) {

		c.Set("config", config)
		c.Next()
	}
}

package main

import (
	"fmt"

	"github.com/bugisdev/mypocket/conf"
	"github.com/bugisdev/mypocket/pkg/db"
	"github.com/bugisdev/mypocket/pkg/middleware"
	"github.com/bugisdev/mypocket/pkg/router"
	"github.com/gin-gonic/gin"
)

func main() {

	// read configuration files
	cfg := conf.ReadConfig()

	if cfg.App.Production {
		gin.SetMode(gin.ReleaseMode)
	}

	// connecting to the database
	dbcon := db.DB(cfg.DB.Driver, cfg.DB.GetCon(), true)

	// collecting middlewares
	middlewares := []middleware.Middleware{
		middleware.Middleware{
			Func: db.ShareDB(dbcon),
		},
		middleware.Middleware{
			Func: middleware.CORS(),
		},
	}

	// create new router
	router := router.NewRouter(middlewares...)

	// run the built-in http server
	router.Run(fmt.Sprintf(":%d", cfg.App.Port))
}

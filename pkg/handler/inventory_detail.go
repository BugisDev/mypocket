package handler

import (
	"net/http"
	"strconv"

	"github.com/bugisdev/mypocket/pkg/inventory"
	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
	"github.com/ngurajeka/nightfury/pkg/error"
	"github.com/ngurajeka/nightfury/pkg/response"
)

func GetInventoryDetails(c *gin.Context) {

	var (
		code = http.StatusOK
		resp = nfresponse.NewResponse()
	)

	db := c.MustGet("db").(*gorm.DB)

	inventoryDetails, errs := inventory.GetInventoryDetails(db)
	resp.Data = inventoryDetails
	resp.Status.TotalRecords = len(inventoryDetails)
	resp.Status.Page = 1

	if errs.IsError() {
		code = 404
		if errs.Message == "" {
			errs.Message = "Records Not Found"
		}

		c.JSON(code, nfresponse.ErrorResponse(errs))
		return
	}

	c.JSON(code, resp.GetResponse())
}

func GetInventoryDetail(c *gin.Context) {

	var (
		code  = http.StatusOK
		db    = c.MustGet("db").(*gorm.DB)
		id, _ = strconv.Atoi(c.Param("id"))
		resp  = nfresponse.NewResponse()
	)

	inventoryDetail, errs := inventory.GetInventoryDetail(db, id)
	resp.Data = inventoryDetail
	resp.Status.Message = "Data Fetched"

	if errs.IsError() {
		code = 404
		if errs.Message == "" {
			errs.Message = "Record Not Found"
		}

		c.JSON(code, nfresponse.ErrorResponse(errs))
		return
	}

	c.JSON(code, resp.GetResponse())
}

func NewInventoryDetail(c *gin.Context) {

	var (
		code  = http.StatusCreated
		db    = c.MustGet("db").(*gorm.DB)
		entry inventory.EntryDetail
		resp  = nfresponse.NewResponse()
	)

	if err := c.Bind(&entry); err != nil {
		code = 409
		errs := nferror.Errors{
			Message: err.Error(),
		}

		c.JSON(code, nfresponse.ErrorResponse(&errs))
		return
	}

	if errs, ok := entry.Check(); !ok {
		code = 409
		nferrs := nferror.Errors{
			Message: "Insufficient Data",
			Errors:  errs,
		}

		c.JSON(code, nfresponse.ErrorResponse(&nferrs))
		return
	}

	// check if header record was found
	_, errs := inventory.GetInventoryHeader(
		db, int(entry.InventoryHeaderID),
	)
	if errs.IsError() {
		code = 404
		nferrs := nferror.Errors{
			Message: "Inventory Header Record Not Found",
			Errors:  errs.Errors,
		}

		c.JSON(code, nfresponse.ErrorResponse(&nferrs))
		return
	}

	inventoryDetail, errs := inventory.NewInventoryDetail(db, entry)
	resp.Data = inventoryDetail
	if errs.IsError() {
		code = 409
		if errs.Message == "" {
			errs.Message = "Error When Creating New Record"
		}

		c.JSON(code, nfresponse.ErrorResponse(errs))
		return
	}

	c.JSON(code, resp.GetResponse())
}

func UpdateInventoryDetail(c *gin.Context) {

	var (
		code  = http.StatusOK
		db    = c.MustGet("db").(*gorm.DB)
		id, _ = strconv.Atoi(c.Param("id"))
		entry inventory.EntryDetail
		resp  = nfresponse.NewResponse()
	)

	// check record exist
	inventoryDetail, errs := inventory.GetInventoryDetail(db, id)

	if err := c.Bind(&entry); err != nil {
		code = 409
		nferrs := nferror.Errors{
			Message: err.Error(),
		}

		c.JSON(code, nfresponse.ErrorResponse(&nferrs))
		return
	}

	if errs.IsError() {
		code = 404
		if errs.Message == "" {
			errs.Message = "Inventory Detail Not Found"
		}

		c.JSON(code, nfresponse.ErrorResponse(errs))
		return
	}

	if inventoryDetail == nil || inventoryDetail.ID == 0 {
		code = 404
		nferrs := nferror.Errors{
			Message: "Record Not Found",
		}

		c.JSON(code, nfresponse.ErrorResponse(&nferrs))
		return
	}

	if errs, ok := entry.Check(); !ok {
		code = 409
		nferrs := nferror.Errors{
			Message: "Insufficient Data",
			Errors:  errs,
		}

		c.JSON(code, nfresponse.ErrorResponse(&nferrs))
		return
	}

	// check if header record was found
	_, errs = inventory.GetInventoryHeader(
		db, int(entry.InventoryHeaderID),
	)
	if errs.IsError() {
		code = 404
		nferrs := nferror.Errors{
			Message: "Inventory Header Record Not Found",
			Errors:  errs.Errors,
		}

		c.JSON(code, nfresponse.ErrorResponse(&nferrs))
		return
	}

	if errs := inventory.UpdateInventoryDetail(
		db, inventoryDetail, entry,
	); errs.IsError() {
		code = 409
		if errs.Message == "" {
			errs.Message = "Error When Updating Record"
		}

		c.JSON(code, nfresponse.ErrorResponse(errs))
		return
	}

	resp.Data = inventoryDetail
	resp.Status.Message = "Data Updated"
	c.JSON(code, resp.GetResponse())
}

func DeleteInventoryDetail(c *gin.Context) {}

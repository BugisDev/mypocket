package handler

import (
	"net/http"
	"strconv"

	"github.com/bugisdev/mypocket/pkg/inventory"
	"github.com/bugisdev/mypocket/pkg/order"
	"github.com/bugisdev/mypocket/pkg/sale"
	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
	"github.com/ngurajeka/nightfury/pkg/error"
	"github.com/ngurajeka/nightfury/pkg/response"
)

func GetOrders(c *gin.Context) {

	var (
		code = http.StatusOK
		resp = nfresponse.NewResponse()
	)

	db := c.MustGet("db").(*gorm.DB)

	orders, errs := order.GetOrders(db)
	resp.Data = orders
	resp.Status.TotalRecords = len(orders)
	resp.Status.Page = 1

	if errs.IsError() {
		code = 404
		if errs.Message == "" {
			errs.Message = "Empty Records"
		}

		c.JSON(code, nfresponse.ErrorResponse(errs))
		return
	}

	c.JSON(code, resp.GetResponse())
}

func GetOrder(c *gin.Context) {

	var (
		code  = http.StatusOK
		db    = c.MustGet("db").(*gorm.DB)
		id, _ = strconv.Atoi(c.Param("id"))
		resp  = nfresponse.NewResponse()
	)

	order, errs := order.GetOrder(db, id)
	resp.Data = order
	resp.Status.Message = "Data Fetched"

	if errs.IsError() {
		code = 404
		if errs.Message == "" {
			errs.Message = "Record Not Found"
		}

		c.JSON(code, nfresponse.ErrorResponse(errs))
		return
	}

	c.JSON(code, resp.GetResponse())
}

func NewOrder(c *gin.Context) {

	var (
		code  = http.StatusCreated
		db    = c.MustGet("db").(*gorm.DB)
		entry order.Entry
		resp  = nfresponse.NewResponse()
	)

	if err := c.Bind(&entry); err != nil {
		code = 409
		nferrs := nferror.Errors{
			Message: err.Error(),
		}

		c.JSON(code, nfresponse.ErrorResponse(&nferrs))
		return
	}

	// if errs, ok := entry.Check(); !ok {
	// 	code = 409
	// 	resp.Status.Error = true
	// 	resp.Status.Errors = nferror.Errors{
	// 		Message: "Insufficient Data",
	// 		Errors:  errs,
	// 	}

	// 	c.JSON(code, resp.GetResponse())
	// 	return
	// }

	// check if header record was found
	inventoryDetail, errs := inventory.GetInventoryDetail(
		db, int(entry.InventoryDetailID),
	)
	if errs.IsError() {
		code = 404
		nferrs := nferror.Errors{
			Message: "Inventory Detail Record Not Found",
			Errors:  errs.Errors,
		}

		c.JSON(code, nfresponse.ErrorResponse(&nferrs))
		return
	}

	if inventoryDetail == nil || inventoryDetail.ID == 0 {
		code = 404
		nferrs := nferror.Errors{
			Message: "Inventory Detail Record Not Found",
			Errors:  errs.Errors,
		}

		c.JSON(code, nfresponse.ErrorResponse(&nferrs))
		return
	}

	result, errs := order.NewOrder(db, entry)
	resp.Data = result
	resp.Status.Message = "Data Created"

	if errs.IsError() {
		code = 409
		if errs.Message == "" {
			errs.Message = "Error When Creating New Record"
		}

		c.JSON(code, nfresponse.ErrorResponse(errs))
		return
	}

	// if saled, we'll create sale data and update inventory stock
	if entry.IsSaled {
		left := inventoryDetail.Quantity - result.Quantity
		nferrs := inventory.UpdateInventoryDetail(
			db, inventoryDetail, inventory.EntryDetail{Quantity: left},
		)

		if nferrs.IsError() {
			code = 409
			if nferrs.Message == "" {
				nferrs.Message = "Updating Inventory Stock Failed"
			}

			c.JSON(code, nfresponse.ErrorResponse(nferrs))
			return
		}

		_, nferrs = sale.NewSale(
			db, sale.Entry{
				CustomerID:        result.CustomerID,
				InventoryDetailID: inventoryDetail.ID,
				OrderID:           result.ID,
				InvoiceNumber:     result.InvoiceNumber,
				Quantity:          result.Quantity,
				Amount:            result.Amount,
			},
		)

		if nferrs.IsError() {
			code = 409
			if nferrs.Message == "" {
				nferrs.Message = "Creating Sale Record Failed"
			}

			c.JSON(code, nfresponse.ErrorResponse(nferrs))
		}
	}

	c.JSON(code, resp.GetResponse())
}

func UpdateOrder(c *gin.Context) {

	var (
		code  = http.StatusOK
		db    = c.MustGet("db").(*gorm.DB)
		id, _ = strconv.Atoi(c.Param("id"))
		entry order.Entry
		resp  = nfresponse.NewResponse()
	)

	// check record exist
	result, errs := order.GetOrder(db, id)

	if errs.IsError() {
		code = 404
		if errs.Message == "" {
			errs.Message = "Order Not Found"
		}

		c.JSON(code, nfresponse.ErrorResponse(errs))
		return
	}

	if result == nil || result.ID == 0 {
		code = 404
		nferrs := nferror.Errors{
			Message: "Record Not Found",
		}

		c.JSON(code, nfresponse.ErrorResponse(&nferrs))
		return
	}

	if err := c.Bind(&entry); err != nil {
		code = 409
		nferrs := nferror.Errors{
			Message: err.Error(),
		}

		c.JSON(code, nfresponse.ErrorResponse(&nferrs))
		return
	}

	// if errs, ok := entry.Check(); !ok {
	// 	code = 409
	// 	resp.Status.Error = true
	// 	resp.Status.Errors = nferror.Errors{
	// 		Message: "Insufficient Data",
	// 		Errors:  errs,
	// 	}

	// 	c.JSON(code, resp.GetResponse())
	// 	return
	// }

	// check if inventory record was found
	inventoryDetail, errs := inventory.GetInventoryDetail(
		db, int(result.InventoryDetailID),
	)
	if errs.IsError() {
		code = 404
		nferrs := nferror.Errors{
			Message: "Inventory Detail Record Not Found",
			Errors:  errs.Errors,
		}

		c.JSON(code, nfresponse.ErrorResponse(&nferrs))
		return
	}

	if inventoryDetail == nil || inventoryDetail.ID == 0 {
		code = 404
		nferrs := nferror.Errors{
			Message: "Inventory Detail Record Not Found",
			Errors:  errs.Errors,
		}

		c.JSON(code, nfresponse.ErrorResponse(&nferrs))
		return
	}

	if errs := order.UpdateOrder(
		db, result, entry,
	); errs.IsError() {
		code = 409
		if errs.Message == "" {
			errs.Message = "Error When Updating Record"
		}

		c.JSON(code, nfresponse.ErrorResponse(errs))
		return
	}

	// if saled, we'll create sale data and update inventory stock
	if entry.IsSaled {
		left := inventoryDetail.Quantity - result.Quantity
		nferrs := inventory.UpdateInventoryDetail(
			db, inventoryDetail, inventory.EntryDetail{Quantity: left},
		)

		if nferrs.IsError() {
			code = 409
			if nferrs.Message == "" {
				nferrs.Message = "Updating Inventory Stock Failed"
			}

			c.JSON(code, nfresponse.ErrorResponse(nferrs))
			return
		}

		_, nferrs = sale.NewSale(
			db, sale.Entry{
				CustomerID:        result.CustomerID,
				InventoryDetailID: inventoryDetail.ID,
				OrderID:           result.ID,
				InvoiceNumber:     result.InvoiceNumber,
				Quantity:          result.Quantity,
				Amount:            result.Amount,
			},
		)

		if nferrs.IsError() {
			code = 409
			if nferrs.Message == "" {
				nferrs.Message = "Creating Sale Record Failed"
			}

			c.JSON(code, nfresponse.ErrorResponse(nferrs))
		}
	}

	resp.Data = result
	resp.Status.Message = "Data Updated"
	c.JSON(code, resp.GetResponse())
}

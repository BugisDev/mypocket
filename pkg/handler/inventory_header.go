package handler

import (
	"net/http"
	"strconv"

	"github.com/bugisdev/mypocket/pkg/inventory"
	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
	"github.com/ngurajeka/nightfury/pkg/error"
	"github.com/ngurajeka/nightfury/pkg/response"
)

func GetInventoryHeaders(c *gin.Context) {

	var (
		code = http.StatusOK
		resp = nfresponse.NewResponse()
	)

	db := c.MustGet("db").(*gorm.DB)

	inventoryHeaders, errs := inventory.GetInventoryHeaders(db)
	resp.Data = inventoryHeaders
	resp.Status.TotalRecords = len(inventoryHeaders)
	resp.Status.Page = 1

	if errs.IsError() {
		code = 404
		if errs.Message == "" {
			errs.Message = "Records Not Found"
		}

		c.JSON(code, nfresponse.ErrorResponse(errs))
		return
	}

	c.JSON(code, resp.GetResponse())
}

func GetInventoryHeader(c *gin.Context) {

	var (
		code  = http.StatusOK
		db    = c.MustGet("db").(*gorm.DB)
		id, _ = strconv.Atoi(c.Param("id"))
		resp  = nfresponse.NewResponse()
	)

	inventoryHeader, errs := inventory.GetInventoryHeader(db, id)
	resp.Data = inventoryHeader
	resp.Status.Message = "Data Fetched"

	if errs.IsError() {
		code = 404
		if errs.Message == "" {
			errs.Message = "Record Not Found"
		}

		c.JSON(code, nfresponse.ErrorResponse(errs))
		return
	}

	c.JSON(code, resp.GetResponse())
}

func NewInventoryHeader(c *gin.Context) {

	var (
		code  = http.StatusCreated
		db    = c.MustGet("db").(*gorm.DB)
		entry inventory.Entry
		resp  = nfresponse.NewResponse()
	)

	if err := c.Bind(&entry); err != nil {
		code = 409
		errs := nferror.Errors{
			Message: err.Error(),
		}

		c.JSON(code, nfresponse.ErrorResponse(&errs))
		return
	}

	if errs, ok := entry.Check(); !ok {
		code = 409
		errs := nferror.Errors{
			Message: "Insufficient Data",
			Errors:  errs,
		}

		c.JSON(code, nfresponse.ErrorResponse(&errs))
		return
	}

	inventoryHeader, errs := inventory.NewInventoryHeader(
		db, entry.Name, entry.Description,
	)
	resp.Data = inventoryHeader
	if errs.IsError() {
		code = 409
		if errs.Message == "" {
			errs.Message = "Error When Creating New Record"
		}

		c.JSON(code, nfresponse.ErrorResponse(errs))
		return
	}

	resp.Status.Message = "Data Created"
	c.JSON(code, resp.GetResponse())
}

func PopulateDefaultInventoryHeaders(c *gin.Context) {

	var (
		code = http.StatusOK
		db   = c.MustGet("db").(*gorm.DB)
		resp = nfresponse.NewResponse()
	)

	inventoryHeaders, errs := inventory.InsertDefaultHeader(db)
	resp.Data = inventoryHeaders
	if errs.IsError() {
		code = 409
		if errs.Message == "" {
			errs.Message = "Populate Data Errors"
		}

		c.JSON(code, nfresponse.ErrorResponse(errs))
		return
	}

	c.JSON(code, resp.GetResponse())
}

func UpdateInventoryHeader(c *gin.Context) {

	var (
		code  = http.StatusOK
		db    = c.MustGet("db").(*gorm.DB)
		id, _ = strconv.Atoi(c.Param("id"))
		entry inventory.Entry
		resp  = nfresponse.NewResponse()
	)

	// check record exist
	inventoryHeader, errs := inventory.GetInventoryHeader(db, id)

	if err := c.Bind(&entry); err != nil {
		code = 409
		errs := nferror.Errors{
			Message: err.Error(),
		}

		c.JSON(code, nfresponse.ErrorResponse(&errs))
		return
	}

	if errs.IsError() {
		code = 409
		if errs.Message == "" {
			errs.Message = "Updating Record Failed"
		}

		c.JSON(code, nfresponse.ErrorResponse(errs))
		return
	}

	if inventoryHeader == nil || inventoryHeader.ID == 0 {
		code = 404
		errs := nferror.Errors{
			Message: "Record Not Found",
		}

		c.JSON(code, nfresponse.ErrorResponse(&errs))
		return
	}

	if errs, ok := entry.Check(); !ok {
		code = 409
		nferrs := nferror.Errors{
			Message: "Insufficient Data",
			Errors:  errs,
		}

		c.JSON(code, nfresponse.ErrorResponse(&nferrs))
		return
	}

	if errs := inventory.UpdateInventoryHeader(
		db, inventoryHeader, entry,
	); errs.IsError() {
		code = 409
		if errs.Message == "" {
			errs.Message = "Updating Record Failed"
		}

		c.JSON(code, nfresponse.ErrorResponse(errs))
		return
	}

	resp.Data = inventoryHeader
	resp.Status.Message = "Data Updated"
	c.JSON(code, resp.GetResponse())
}

func DeleteInventoryHeader(c *gin.Context) {}

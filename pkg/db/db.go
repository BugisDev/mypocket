package db

import (
	"log"

	"github.com/bugisdev/mypocket/pkg/model"
	"github.com/gin-gonic/gin"
	_ "github.com/go-sql-driver/mysql"
	"github.com/jinzhu/gorm"
)

func DB(driver, connection string, logMode bool) *gorm.DB {

	db, err := gorm.Open(driver, connection)
	if err != nil {

		log.Panicf("Panic With Error: %s", err.Error())
	}

	db.LogMode(logMode)
	AutoMigrate(db)

	return db
}

func ShareDB(db *gorm.DB) gin.HandlerFunc {

	return func(c *gin.Context) {

		c.Set("db", db)
		c.Next()

	}
}

func AutoMigrate(db *gorm.DB) {

	db.AutoMigrate(
		&model.KBBI{},
		&model.InventoryHeader{},
		&model.InventoryDetail{},
		&model.Customer{},
		&model.Order{},
		&model.Sale{},
	)

}

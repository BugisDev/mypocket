package middleware

import "github.com/gin-gonic/gin"

type Middleware struct {
	Func gin.HandlerFunc
}

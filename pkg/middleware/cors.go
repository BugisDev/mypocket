package middleware

import "github.com/gin-gonic/gin"

func CORS() gin.HandlerFunc {

	return func(c *gin.Context) {

		c.Header("Access-Control-Allow-Credentials", "true")
		c.Header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Range, Content-Disposition, Content-Type, Authorization")
		c.Header("Access-Control-Allow-Methods", "GET,PATCH,PUT,POST,DELETE,OPTIONS")
		c.Header("Access-Control-Allow-Origin", "*")
		c.Next()
	}
}

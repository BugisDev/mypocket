package router

import (
	"net/http"

	"github.com/bugisdev/mypocket/pkg/handler"
	"github.com/bugisdev/mypocket/pkg/middleware"
	"github.com/gin-gonic/gin"
)

func NewRouter(middlewares ...middleware.Middleware) *gin.Engine {

	// default router
	r := gin.Default()

	// registering middlewares
	RegisterMiddlewares(r, middlewares)

	// define routes
	DefineRoutes(r)

	return r
}

func RegisterMiddlewares(r *gin.Engine, middlewares []middleware.Middleware) {

	for _, middleware := range middlewares {

		r.Use(middleware.Func)

	}

}

func DefineRoutes(r *gin.Engine) {

	r.GET("/", func(c *gin.Context) {
		c.JSON(http.StatusOK, gin.H{"status": 200, "message": "OK"})
	})

	api := r.Group("/api")
	{
		inventory := api.Group("/inventory")
		{
			// header
			inventory.GET("/header", handler.GetInventoryHeaders)
			inventory.GET("/header/:id", handler.GetInventoryHeader)
			inventory.POST("/header", handler.NewInventoryHeader)
			inventory.POST("/header/:id", handler.UpdateInventoryHeader)
			inventory.PUT("/header/:id", handler.UpdateInventoryHeader)
			inventory.PATCH("/header/:id", handler.UpdateInventoryHeader)
			inventory.DELETE("/header/:id", handler.DeleteInventoryHeader)

			// detail
			inventory.GET("/detail", handler.GetInventoryDetails)
			inventory.GET("/detail/:id", handler.GetInventoryDetail)
			inventory.POST("/detail", handler.NewInventoryDetail)
			inventory.POST("/detail/:id", handler.UpdateInventoryDetail)
			inventory.PUT("/detail/:id", handler.UpdateInventoryDetail)
			inventory.PATCH("/detail/:id", handler.UpdateInventoryDetail)
			inventory.DELETE("/detail/:id", handler.DeleteInventoryDetail)

			// populate
			// inventory.GET(
			// 	"/populate_default/header",
			// 	handler.PopulateDefaultInventoryHeaders,
			// )

		}

		order := api.Group("/order")
		{

			order.GET("", handler.GetOrders)
			order.GET("/:id", handler.GetOrder)
			order.POST("", handler.NewOrder)
			order.PUT("/:id", handler.UpdateOrder)

		}
	}

}

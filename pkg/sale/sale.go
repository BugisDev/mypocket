package sale

import (
	"github.com/bugisdev/mypocket/pkg/model"
	"github.com/jinzhu/gorm"
	"github.com/ngurajeka/nightfury/pkg/error"
)

type Entry struct {
	CustomerID        uint
	InventoryDetailID uint
	OrderID           uint
	InvoiceNumber     string
	Quantity          int
	Amount            float64
}

func NewSale(db *gorm.DB, entry Entry) (*model.Sale, *nferror.Errors) {

	var errs = nferror.Empty()

	sale := model.Sale{
		OrderID:           entry.OrderID,
		InventoryDetailID: entry.InventoryDetailID,
		CustomerID:        entry.CustomerID,
		InvoiceNumber:     entry.InvoiceNumber,
		Quantity:          entry.Quantity,
		Amount:            entry.Amount,
	}

	if err := db.Create(&sale).Error; err != nil {

		errs = errs.Append(409, "", err.Error(), nil)
	}

	return &sale, errs
}

func GetSales(db *gorm.DB) ([]model.Sale, *nferror.Errors) {

	var (
		errs  = nferror.Empty()
		sales []model.Sale
	)

	if err := db.Find(&sales).Error; err != nil {

		errs = errs.Append(404, "", err.Error(), nil)
	}

	return sales, errs
}

func GetSale(db *gorm.DB, id int) (*model.Sale, *nferror.Errors) {

	var (
		errs = nferror.Empty()
		sale model.Sale
	)

	db = db.Where("id = ?", id)

	if err := db.First(&sale).Error; err != nil {

		errs = errs.Append(404, "", err.Error(), nil)
	}

	return &sale, errs
}

func GetSalesByInventoryDetailID(db *gorm.DB, id int) (
	[]model.Sale, *nferror.Errors,
) {

	var (
		errs  = nferror.Empty()
		sales []model.Sale
	)

	db = db.Where(model.Sale{InventoryDetailID: uint(id)})

	if err := db.Find(&sales).Error; err != nil {

		errs = errs.Append(404, "", err.Error(), nil)
	}

	return sales, errs
}

package inventory

import (
	"github.com/bugisdev/mypocket/pkg/model"
	"github.com/jinzhu/gorm"
	"github.com/ngurajeka/nightfury/pkg/error"
)

//NewInventoryHeader create new inventory header or type
func NewInventoryHeader(db *gorm.DB, name, desc string) (
	*model.InventoryHeader, *nferror.Errors,
) {

	var errs = nferror.Empty()

	inventoryHeader := model.InventoryHeader{Name: name, Description: desc}

	if err := db.Create(&inventoryHeader).Error; err != nil {

		errs = errs.Append(409, "", err.Error(), nil)
	}

	return &inventoryHeader, errs
}

//GetInventoryHeaders return all inventory headers
func GetInventoryHeaders(db *gorm.DB) (
	[]model.InventoryHeader, *nferror.Errors,
) {

	var (
		errs             = nferror.Empty()
		inventoryHeaders []model.InventoryHeader
	)

	if err := db.Find(&inventoryHeaders).Error; err != nil {

		errs = errs.Append(409, "", err.Error(), nil)
	}

	return inventoryHeaders, errs
}

//GetInventoryHeader return single inventory header
func GetInventoryHeader(db *gorm.DB, id int) (
	*model.InventoryHeader, *nferror.Errors,
) {

	var (
		errs            = nferror.Empty()
		inventoryHeader model.InventoryHeader
	)

	db = db.Where("id = ?", id)

	if err := db.First(&inventoryHeader).Error; err != nil {

		errs = errs.Append(409, "", err.Error(), nil)
	}

	return &inventoryHeader, errs
}

//UpdateInventoryHeader updating single inventory header
func UpdateInventoryHeader(
	db *gorm.DB, inventoryHeader *model.InventoryHeader, entry Entry,
) *nferror.Errors {

	var (
		errs = nferror.Empty()
	)

	if err := db.Model(inventoryHeader).Update(
		model.InventoryHeader{Name: entry.Name, Description: entry.Description},
	).Error; err != nil {

		errs = errs.Append(409, "", err.Error(), nil)
	}

	return errs
}

package inventory

import (
	"github.com/bugisdev/mypocket/pkg/model"
	"github.com/jinzhu/gorm"
	"github.com/ngurajeka/nightfury/pkg/error"
)

//NewInventoryDetail create new inventory detail
func NewInventoryDetail(db *gorm.DB, entry EntryDetail) (
	*model.InventoryDetail, *nferror.Errors,
) {

	var errs = nferror.Empty()

	inventoryDetail := model.InventoryDetail{
		Name:              entry.Name,
		Code:              entry.Code,
		Description:       entry.Description,
		Quantity:          entry.Quantity,
		Price:             entry.Price,
		InventoryHeaderID: entry.InventoryHeaderID,
	}

	if err := db.Create(&inventoryDetail).Error; err != nil {

		errs = errs.Append(409, "", err.Error(), nil)
	}

	return &inventoryDetail, errs
}

//GetInventoryDetails return all inventory details
func GetInventoryDetails(db *gorm.DB) (
	[]model.InventoryDetail, *nferror.Errors,
) {

	var (
		errs             = nferror.Empty()
		inventoryDetails []model.InventoryDetail
	)

	if err := db.Find(&inventoryDetails).Error; err != nil {

		errs = errs.Append(409, "", err.Error(), nil)
	}

	return inventoryDetails, errs
}

//GetInventoryDetail return single inventory detail
func GetInventoryDetail(db *gorm.DB, id int) (
	*model.InventoryDetail, *nferror.Errors,
) {

	var (
		errs            = nferror.Empty()
		inventoryDetail model.InventoryDetail
	)

	db = db.Where("id = ?", id)

	if err := db.First(&inventoryDetail).Error; err != nil {

		errs = errs.Append(409, "", err.Error(), nil)
	}

	return &inventoryDetail, errs
}

//UpdateInventoryDetail updating single inventory header
func UpdateInventoryDetail(
	db *gorm.DB, inventoryDetail *model.InventoryDetail, entry EntryDetail,
) *nferror.Errors {

	var (
		errs = nferror.Empty()
	)

	if err := db.Model(inventoryDetail).Update(
		model.InventoryDetail{
			Name:        entry.Name,
			Code:        entry.Code,
			Description: entry.Description,
			Quantity:    entry.Quantity,
			Price:       entry.Price,
		},
	).Error; err != nil {

		errs = errs.Append(409, "", err.Error(), nil)
	}

	return errs
}

func CountAmount(db *gorm.DB, id int) (float64, *nferror.Errors) {

	var (
		errs   = nferror.Empty()
		amount float64
	)

	detail, err := GetInventoryDetail(db, id)
	if err.IsError() {
		errs = errs.AppendErrors(err)
	} else {
		amount += detail.Price
	}

	return amount, errs
}

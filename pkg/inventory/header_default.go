package inventory

import (
	"github.com/bugisdev/mypocket/pkg/model"
	"github.com/jinzhu/gorm"
	"github.com/ngurajeka/nightfury/pkg/error"
)

const (
	CLOTHES     = "Pakaian"
	ACCESSORIES = "Aksesoris"
)

//InsertDefaultHeader create default data for inventory header / type
func InsertDefaultHeader(db *gorm.DB) (
	[]*model.InventoryHeader, *nferror.Errors,
) {

	var (
		errs             = nferror.Empty()
		inventoryHeaders []*model.InventoryHeader
	)

	for _, header := range []string{CLOTHES, ACCESSORIES} {

		inventoryHeader, err := NewInventoryHeader(db, header, "")

		if err != nil {

			errs = errs.AppendErrors(errs)
		}

		inventoryHeaders = append(inventoryHeaders, inventoryHeader)

	}

	return inventoryHeaders, errs
}

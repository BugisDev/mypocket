package inventory

import (
	"fmt"

	"github.com/ngurajeka/nightfury/pkg/error"
)

const (
	MAX_CHAR = 32
	MIN_CHAR = 3
)

type Entry struct {
	Name, Description string
}

func (e *Entry) Check() ([]nferror.Error, bool) {

	var (
		ok   = true
		errs []nferror.Error
	)

	if e.Name == "" {
		ok = false
		errs = append(errs, nferror.Error{
			Code:    404,
			Field:   "Name",
			Message: "Field Name is required",
		})
	}

	if len(e.Name) > MAX_CHAR {
		ok = false
		errs = append(errs, nferror.Error{
			Code:  409,
			Field: "Name",
			Message: fmt.Sprintf(
				"Field Name Should be less than %d Character", MAX_CHAR,
			),
		})
	}

	if len(e.Name) < MIN_CHAR {
		ok = false
		errs = append(errs, nferror.Error{
			Code:  409,
			Field: "Name",
			Message: fmt.Sprintf(
				"Field Name Should be greater than %d Character", MIN_CHAR,
			),
		})
	}

	if e.Description == "" {
		ok = false
		errs = append(errs, nferror.Error{
			Code:    404,
			Field:   "Description",
			Message: "Field Description is required",
		})
	}

	return errs, ok
}

type EntryDetail struct {
	Name, Code, Description string
	Quantity                int     `json:"Quantity,string"`
	InventoryHeaderID       uint    `json:"InventoryHeaderID,string"`
	Price                   float64 `json:"Price,string"`
}

func (e *EntryDetail) Check() ([]nferror.Error, bool) {

	var (
		ok   = true
		errs []nferror.Error
	)

	if e.Name == "" {
		ok = false
		errs = append(errs, nferror.Error{
			Code:    404,
			Field:   "Name",
			Message: "Field Name is required",
		})
	}

	if len(e.Name) > MAX_CHAR {
		ok = false
		errs = append(errs, nferror.Error{
			Code:  409,
			Field: "Name",
			Message: fmt.Sprintf(
				"Field Name Should be less than %d Character", MAX_CHAR,
			),
		})
	}

	if len(e.Name) < MIN_CHAR {
		ok = false
		errs = append(errs, nferror.Error{
			Code:  409,
			Field: "Name",
			Message: fmt.Sprintf(
				"Field Name Should be greater than %d Character", MIN_CHAR,
			),
		})
	}

	if e.Code == "" {
		ok = false
		errs = append(errs, nferror.Error{
			Code:    404,
			Field:   "Code",
			Message: "Field Code is required",
		})
	}

	if len(e.Code) > MAX_CHAR {
		ok = false
		errs = append(errs, nferror.Error{
			Code:  409,
			Field: "Code",
			Message: fmt.Sprintf(
				"Field Code Should be less than %d Character", MAX_CHAR,
			),
		})
	}

	if len(e.Code) < MIN_CHAR {
		ok = false
		errs = append(errs, nferror.Error{
			Code:  409,
			Field: "Code",
			Message: fmt.Sprintf(
				"Field Code Should be greater than %d Character", MIN_CHAR,
			),
		})
	}

	if e.Description == "" {
		ok = false
		errs = append(errs, nferror.Error{
			Code:    404,
			Field:   "Description",
			Message: "Field Description is required",
		})
	}

	if e.InventoryHeaderID == 0 {
		ok = false
		errs = append(errs, nferror.Error{
			Code:    409,
			Field:   "InventoryHeaderID",
			Message: "Field Inventory Header Should be an ID",
		})
	}

	if e.Price == float64(0) {
		ok = false
		errs = append(errs, nferror.Error{
			Code:    409,
			Field:   "Price",
			Message: "Field Price Should be greater than 0 (zero)",
		})
	}

	return errs, ok
}

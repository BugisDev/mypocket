package customer

import (
	"github.com/bugisdev/mypocket/pkg/model"
	"github.com/jinzhu/gorm"
	"github.com/ngurajeka/nightfury/pkg/error"
)

type Entry struct {
	Name string
}

func NewCustomer(db *gorm.DB, entry Entry) (*model.Customer, *nferror.Errors) {

	var errs = nferror.Empty()

	customer := model.Customer{Name: entry.Name}

	if err := db.Create(&customer).Error; err != nil {

		errs.Append(409, "", err.Error(), nil)
	}

	return &customer, errs
}

package model

import (
	"time"

	"github.com/jinzhu/gorm"
)

type Sale struct {
	gorm.Model
	OrderID           uint
	Order             Order `json:"-"`
	InventoryDetailID uint
	InventoryDetail   InventoryDetail `json:"-"`
	CustomerID        uint
	Customer          Customer `json:"-"`
	InvoiceNumber     string
	Quantity          int
	Amount            float64
}

func (m *Sale) TableName() string {

	return "sale"
}

func (m *Sale) GetId() uint {

	return m.ID
}

func (m *Sale) GetCreatedAt() time.Time {

	return m.CreatedAt
}

func (m *Sale) GetUpdatedAt() time.Time {

	return m.UpdatedAt
}

package model

import (
	"time"

	"github.com/jinzhu/gorm"
)

//InventoryHeader is a collection of the inventory type(s)
type InventoryHeader struct {
	gorm.Model
	Name, Description string
}

//TableName set table name
func (m *InventoryHeader) TableName() string {

	return "inventory_header"
}

//GetID get id from model
func (m *InventoryHeader) GetId() uint {

	return m.ID
}

//GetCreatedAt get created time from model
func (m *InventoryHeader) GetCreatedAt() time.Time {

	return m.CreatedAt
}

//GetUpdatedAt get updated time from model
func (m *InventoryHeader) GetUpdatedAt() time.Time {

	return m.UpdatedAt
}

//InventoryDetail is a collection of the inventory detail per type(s)
type InventoryDetail struct {
	gorm.Model
	Name, Code, Description string
	Quantity                int
	Price                   float64
	InventoryHeader         InventoryHeader `json:"-" gorm:"ForeignKey:InventoryHeaderID"`
	InventoryHeaderID       uint
}

//TableName set table name
func (m *InventoryDetail) TableName() string {

	return "inventory_detail"
}

//GetID get id from model
func (m *InventoryDetail) GetId() uint {

	return m.ID
}

//GetCreatedAt get created time from model
func (m *InventoryDetail) GetCreatedAt() time.Time {

	return m.CreatedAt
}

//GetUpdatedAt get updated time from model
func (m *InventoryDetail) GetUpdatedAt() time.Time {

	return m.UpdatedAt
}

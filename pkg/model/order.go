package model

import (
	"time"

	"github.com/jinzhu/gorm"
)

type Order struct {
	gorm.Model
	InventoryDetailID uint
	InventoryDetail   InventoryDetail `json:"-"`
	CustomerID        uint
	Customer          Customer `json:"-"`
	InvoiceNumber     string
	Quantity          int
	Amount            float64
	IsSaled           bool
	ExpiredAt         time.Time
}

func (m *Order) TableName() string {

	return "order"
}

func (m *Order) GetId() uint {

	return m.ID
}

func (m *Order) GetCreatedAt() time.Time {

	return m.CreatedAt
}

func (m *Order) GetUpdatedAt() time.Time {

	return m.UpdatedAt
}

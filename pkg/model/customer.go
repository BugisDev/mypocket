package model

import (
	"time"

	"github.com/jinzhu/gorm"
)

type Customer struct {
	gorm.Model
	Name string
}

//TableName set table name
func (m *Customer) TableName() string {

	return "customer"
}

//GetID get id from model
func (m *Customer) GetId() uint {

	return m.ID
}

//GetCreatedAt get created time from model
func (m *Customer) GetCreatedAt() time.Time {

	return m.CreatedAt
}

//GetUpdatedAt get updated time from model
func (m *Customer) GetUpdatedAt() time.Time {

	return m.UpdatedAt
}

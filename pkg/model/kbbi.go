package model

import "github.com/jinzhu/gorm"

type KBBI struct {
	gorm.Model
	Word string
	Type string
}

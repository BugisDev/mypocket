package order

import (
	"fmt"
	"strconv"
	"time"

	"github.com/bugisdev/mypocket/pkg/customer"
	"github.com/bugisdev/mypocket/pkg/inventory"
	"github.com/bugisdev/mypocket/pkg/model"
	"github.com/jinzhu/gorm"
	"github.com/ngurajeka/nightfury/pkg/error"
)

type Entry struct {
	CustomerName      string
	InventoryDetailID uint `json:"InventoryDetailID,string"`
	Quantity          int  `json:"Quantity,string"`
	IsSaled           bool `json:"IsSaled,string"`
	ExpiredAt         time.Time
}

const INVOICE = "INV"

func GenInvoiceNumber(db *gorm.DB) string {

	var invoiceNumber string

	for {

		now := time.Now().Unix()
		suffix := strconv.FormatInt(now, 10)[3:]

		var kbbi model.KBBI
		err := db.Order("RAND()").First(&kbbi).Error
		if err != nil {
			invoiceNumber = fmt.Sprintf("%v%v", INVOICE, suffix)
		}

		if kbbi.ID == 0 {
			invoiceNumber = fmt.Sprintf("%v%v", INVOICE, suffix)
		} else {
			invoiceNumber = fmt.Sprintf("%v%v", kbbi.Word, suffix)
		}

		var order model.Order

		err = db.Where(
			model.Order{InvoiceNumber: invoiceNumber},
		).First(&order).Error

		if err != nil {
			break
		}

		if order.ID == 0 {
			break
		}

	}

	return invoiceNumber
}

func NewOrder(db *gorm.DB, entry Entry) (*model.Order, *nferror.Errors) {

	var errs = nferror.Empty()

	invoiceNumber := GenInvoiceNumber(db)

	// insert new customer
	customer, err := customer.NewCustomer(
		db, customer.Entry{Name: entry.CustomerName},
	)

	if err.IsError() {
		errs = errs.AppendErrors(err)
		return nil, errs
	}

	// count amount
	amount, err := inventory.CountAmount(db, int(entry.InventoryDetailID))
	if err.IsError() {
		errs = errs.AppendErrors(err)
		return nil, errs
	}

	// insert new order
	order := model.Order{
		InventoryDetailID: entry.InventoryDetailID,
		CustomerID:        customer.ID,
		InvoiceNumber:     invoiceNumber,
		Quantity:          entry.Quantity,
		Amount:            amount,
		IsSaled:           entry.IsSaled,
		ExpiredAt:         entry.ExpiredAt,
	}

	if err := db.Create(&order).Error; err != nil {

		errs = errs.Append(409, "", err.Error(), nil)
	}

	return &order, errs
}

func GetOrders(db *gorm.DB) (
	[]model.Order, *nferror.Errors,
) {

	var (
		errs   = nferror.Empty()
		orders []model.Order
	)

	if err := db.Find(&orders).Error; err != nil {

		errs = errs.Append(404, "", err.Error(), nil)
	}

	return orders, errs
}

func GetOrder(db *gorm.DB, id int) (
	*model.Order, *nferror.Errors,
) {

	var (
		errs  = nferror.Empty()
		order model.Order
	)

	db = db.Where("id = ?", id)

	if err := db.First(&order).Error; err != nil {

		errs = errs.Append(404, "", err.Error(), nil)
	}

	return &order, errs
}

func UpdateOrder(
	db *gorm.DB, order *model.Order, entry Entry,
) *nferror.Errors {

	var (
		errs = nferror.Empty()
	)

	if err := db.Model(order).Update(
		model.Order{
			IsSaled: entry.IsSaled,
		},
	).Error; err != nil {

		errs = errs.Append(409, "", err.Error(), nil)
	}

	return errs
}
